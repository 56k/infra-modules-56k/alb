/*
Outputs from the 'alb' module
*/

output "application-lb-name" {
  value = aws_lb.application_lb.dns_name
}

output "application-lb-zone-id" {
  value = aws_lb.application_lb.zone_id
}
